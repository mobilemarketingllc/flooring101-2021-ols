<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.min.js", "", "", 1);
    wp_enqueue_script("child-script", get_stylesheet_directory_uri() . "/script.min.js", "", "", 1);
});

if (current_user_can('manage_options')) {
    add_filter('show_admin_bar', '__return_true');
}



//FacetWP includes built-in accessibility (a11y) support but it’s disabled by default. To enable it
add_filter('facetwp_load_a11y', '__return_true');

// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');



// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');

/*
function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}*/



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id = 0, $size = "", $url = false, $attr = "")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img = get_stylesheet_directory_uri() . "/images/" . $id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="' . $img . '" ' . ($attr ? build_attr($attr) : "") . '>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id = get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id = $id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id) != "attachment") {
        $id = get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url = wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4) == ".svg") {
                $image_url = str_replace(get_bloginfo("url"), ABSPATH . "/", $image_url);
                $data = file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } else if ($url) {
            return $image_url;
        }
    }
}

//Facet Title Hook
// add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
//     if ( isset( $atts['facet'] ) ) {       
//         $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
//     }
//     return $output;
// }, 10, 2 );


function featured_products_fun()
{
    $args = array(
        'post_type' => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
        'post__not_in' => array(323965, 302354),
        'posts_per_page' => 20,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured',
                'value' => true,
                'compare' => '=='
            )
        )
    );
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post();
            // $gallery_images = get_field('gallery_room_images');
            // $gallery_img = explode("|",$gallery_images);
            // $count = 0;
            // // loop through the rows of data
            // foreach($gallery_img as  $key=>$value) {
            //     $room_image = $value;
            //     if(!$room_image){
            //         $room_image = "http://placehold.it/245x310?text=No+Image";
            //     }
            //     else{
            //         if(strpos($room_image , 's7.shawimg.com') !== false){
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "http://" . $room_image;
            //             }
            //             $room_image = $room_image ;
            //         } else{
            //             if(strpos($room_image , 'http') === false){ 
            //                 $room_image = "https://" . $room_image;
            //             }
            //             $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
            //         }
            //     }
            //     if($count>0){
            //         break;
            //     }
            //     $count++;
            // }

            $image = get_field('swatch_image_link') ? get_field('swatch_image_link') : "http://placehold.it/168x123?text=No+Image";

            if (strpos($image, 's7.shawimg.com') !== false) {
                if (strpos($image, 'http') === false) {
                    $image = "http://" . $image;
                }
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[" . $image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[" . $image . "]&scale=size[300x300]&sink";
            } else {
                if (strpos($image, 'http') === false) {
                    $image = "https://" . $image;
                }
                $image_thumb = "https://mobilem.liquifire.com/mobilem?source=url[" . $image . "]&scale=size[150]&sink";
                $image = "https://mobilem.liquifire.com/mobilem?source=url[" . $image . "]&scale=size[600x400]&sink";
            }

            $color_name = get_field('color');
            $post_type = get_post_type_object(get_post_type(get_the_ID()));


            $brand = get_field('brand');
            $collection = get_field('collection');

            $productName = $brand . ' ' . $collection;

            $in_stock = get_field('in_stock', get_the_ID());
            if ($in_stock == 1) {
                $in_stock_txt = '<span class="stock-txt">Best Value</span>';
            } else {
                $in_stock_txt = '';
            }

            $familysku = get_post_meta(get_the_ID(), 'collection', true);
            $args1 = array(
                'post_type'      => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
                'posts_per_page' => -1,
                'post_status'    => 'publish',
                'meta_query'     => array(
                    array(
                        'key'     => 'collection',
                        'value'   => $familysku,
                        'compare' => '='
                    )
                )
            );
            $the_query = new WP_Query($args1);


            $outpout .= '<div class="featured-product-item">
                <div class="featured-inner">
                    <div class="prod-img-wrapper">
                        ' . $in_stock_txt . '
                        <a href="' . get_the_permalink() . '" class="prod-img-wrap"><img src="' . $image . '" alt="' . get_the_title() . '" /></a>
                    </div>
                    <div class="product-info">
                        <h2><a href="' . get_the_permalink() . '">' . $color_name . '</a></h2>
                        <h5>' . $brand . '</h5>
                        <h4>' . $the_query->found_posts . ' Colors</h4>
                    </div>
                </div>
            </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }


    return $outpout;
}
add_shortcode('featured_products', 'featured_products_fun');

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter('wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail', 90);

function wpse_override_yoast_breadcrumb_trail($links)
{

    if (is_singular('carpeting')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/browse-carpet/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('hardwood_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/hardwood/',
            'text' => 'Hardwood Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/browse-hardwood/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('laminate_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/laminate/',
            'text' => 'Laminate Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/browse-laminate/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('luxury_vinyl_tile')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/luxury-vinyl/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/browse-luxury-vinyl/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }
    if (is_singular('tile_catalog')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/flooring-products/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/browse-ceramic/',
            'text' => 'Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    return $links;
}



function featured_products_slider_fun($atts)
{

    // print_r($atts);

    $args = array(
        'post_type' => $atts['0'],
        'posts_per_page' => 8,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured',
                'value' => '1',
                'compare' => '=='
            )
        )
    );
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post();
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|", $gallery_images);

            // loop through the rows of data

            $room_image =  single_gallery_image_product(get_the_ID(), '600', '400');


            $color_name = get_field('color');
            $brand_name = get_field('brand');
            $collection = get_field('collection');
            $post_type = get_post_type_object(get_post_type(get_the_ID()));


            $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="' . $room_image . '" alt="' . get_the_title() . '" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="' . get_the_permalink() . '" class="button alt viewproduct-btn">View Product</a>';

            if (get_option('getcouponbtn') == 1) {

                $outpout .= '<a href="/flooring-coupon/" class="button fea-coupon_btn">Get Coupon</a>';
            }

            $outpout .= '</div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="' . get_the_permalink() . '">' . $collection . '</a></div>
                         <h3 class="floorte-color">' . $color_name . '</h3>
                     </div>
                 </div>
             </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }


    return $outpout;
}
add_shortcode('featured_products_slide', 'featured_products_slider_fun');

add_filter('fl_builder_module_attributes', function ($attrs, $row) {
    if ('test111' == $row->settings->id) {
        $attrs['data-foo'] = 'bar';
    }
    return $attrs;
}, 10, 2);

add_filter('fl_builder_row_attributes', function ($attrs, $row) {
    if ('test341' == $row->settings->id) {
        $attrs['data-foo'] = 'bar';
    }
    return $attrs;
}, 10, 2);



//  IP location FUnctionality
//   if (!wp_next_scheduled('cde_preferred_location_cronjob')) {

//     wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
// }

add_action('cde_preferred_location_cronjob', 'cde_preferred_location');

function cde_preferred_location()
{

    global $wpdb;

    if (! function_exists('post_exists')) {
        require_once(ABSPATH . 'wp-admin/includes/post.php');
    }

    //CALL Authentication API:
    $apiObj = new APICaller;
    $inputs = array('grant_type' => 'client_credentials', 'client_id' => get_option('CLIENT_CODE'), 'client_secret' => get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL, "POST", $inputs, array(), AUTH_BASE_URL);


    if (isset($result['error'])) {
        $msg = $result['error'];
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] = $result['error_description'];
    } else if (isset($result['access_token'])) {

        //API Call for getting website INFO
        $inputs = array();
        $headers = array('authorization' => "bearer " . $result['access_token']);
        $website = $apiObj->call(BASEURL . get_option('SITE_CODE'), "GET", $inputs, $headers);

        //   //write_log($website['result']['locations']);

        for ($i = 0; $i < count($website['result']['locations']); $i++) {

            if ($website['result']['locations'][$i]['type'] == 'store') {

                $location_name = isset($website['result']['locations'][$i]['city']) ? $website['result']['locations'][$i]['city'] : "";

                $found_post = post_exists($location_name, '', '', 'store-locations');

                if ($found_post == 0) {

                    $array = array(
                        'post_title' => $location_name,
                        'post_type' => 'store-locations',
                        'post_content'  => "",
                        'post_status'   => 'publish',
                        'post_author'   => 0,
                    );
                    $post_id = wp_insert_post($array);

                    //   //write_log( $location_name.'---'.$post_id);

                    update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']);
                } else {

                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']);
                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']);
                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']);
                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']);
                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']);
                    if ($website['result']['locations'][$i]['forwardingPhone'] == '') {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);
                    } else {

                        update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);
                    }

                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']);
                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']);
                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']);
                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']);
                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']);
                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']);
                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']);
                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']);
                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']);
                }
            }
        }
    }
}



//get ipaddress of visitor

function getUserIpAddr()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';

    if (strstr($ipaddress, ',')) {
        $tmp = explode(',', $ipaddress, 2);
        $ipaddress = trim($tmp[1]);
    }
    return $ipaddress;
}


// Custom function for lat and long

function get_storelisting()
{

    global $wpdb;
    $content = "";
    $content_list = "";

    //  echo 'no cookie';

    $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
    //12.68.65.195
    $response = wp_remote_post(
        $urllog,
        array(
            'method' => 'GET',
            'timeout' => 45,
            'redirection' => 5,
            'httpversion' => '1.0',
            'headers' => array('Content-Type' => 'application/json'),
            'body' =>  array('u' => 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0', 'ip' => getUserIpAddr(), 'dbs' => 'all', 'trans_id' => 'example', 'json' => 'true'),
            'blocking' => true,
            'cookies' => array()
        )
    );

    $rawdata = json_decode($response['body'], true);
    $userdata = $rawdata['response'];

    $autolat = $userdata['pulseplus-latitude'];
    $autolng = $userdata['pulseplus-longitude'];
    if (isset($_COOKIE['preferred_store'])) {

        $store_location = $_COOKIE['preferred_store'];
    } else {

        $store_location = '';
    }

    //print_r($rawdata);

    $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(" . $autolat . " ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( " . $autolng . " ) ) + sin( radians( " . $autolat . " ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

    //   //write_log($sql);

    $storeposts = $wpdb->get_results($sql);
    $storeposts_array = json_decode(json_encode($storeposts), true);


    if ($store_location == '') {
        ////write_log('empty loc');           
        $store_location = $storeposts_array['0']['ID'];
        $store_distance = round($storeposts_array['0']['distance'], 1);
    } else {

        // //write_log('noempty loc');
        $key = array_search($store_location, array_column($storeposts_array, 'ID'));
        $store_distance = round($storeposts_array[$key]['distance'], 1);
    }


    $content = get_the_title($store_location) . ', ' . get_field('state', $store_location);
    $format_phone = formatPhoneNumber(get_field('phone', $store_location));
    $text_phone = get_field('phone', $store_location);

    foreach ($storeposts as $post) {

        // //write_log(get_field('phone', $post->ID));

        $content_list .= '<div class="location-section" id ="' . $post->ID . '">
                <div class="location-info-section">
                    <h4 class="location-name">' . get_the_title($post->ID) . ' - <b>' . round($post->distance, 1) . ' MI</b></h4>
                    <p class="store-add"> ' . get_field('address', $post->ID) . '<br>' . get_field('city', $post->ID) . ', ' . get_field('state', $post->ID) . ' ' . get_field('postal_code', $post->ID) . '</p>
                    <p class="store-phone"><i class="fa fa-phone-alt"></i> &nbsp;' . formatPhoneNumber(get_field('phone', $post->ID)) . '</p>
                    <div class="store-hour-section">
                        <div class="label">STORE HOURS</div>
                        <ul class="storename"><li><div class="store-opening-hrs-container">
                        <ul class="store-opening-hrs">
                            <li>Monday: <span>' . get_field('monday', $post->ID) . '</span></li>
                            <li>Tuesday: <span>' . get_field('tuesday', $post->ID) . '</span></li>
                            <li>Wednesday: <span>' . get_field('wednesday', $post->ID) . '</span></li>
                            <li>Thursday: <span>' . get_field('thursday', $post->ID) . '</span></li>
                            <li>Friday: <span>' . get_field('friday', $post->ID) . '</span></li>
                            <li>Saturday: <span>' . get_field('saturday', $post->ID) . '</span></li>
                            <li>Sunday: <span>' . get_field('sunday', $post->ID) . '</span></li>
                        </ul>
                        </div></li></ul>
                    </div>
                    ' . do_shortcode('[storelocation_address "dir" "' . get_the_title($post->ID) . '"]') . '
                    <a href="' . get_field('store_link', $post->ID) . '"  class="fl-button-link" role="button"> View Location</a>
                    <a href="javascript:void(0)" data-id="' . $post->ID . '" data-storename="' . get_the_title($post->ID) . '" data-fphone="' . formatPhoneNumber(get_field('phone', $post->ID)) . '" data-phone="' . get_field('phone', $post->ID) . '" data-distance="' . round($post->distance, 1) . '" target="_self" role="button" class="fl-button choose_location">Choose This Location</a>
                </div>
            </div>';
    }


    $data = array();

    $data['header'] = $content;
    $data['formatphone'] = $format_phone;
    $data['text_phone'] = $text_phone;
    $data['list'] = $content_list;

    ////write_log($data['list']);

    echo json_encode($data);
    wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action('wp_ajax_nopriv_get_storelisting', 'get_storelisting');
add_action('wp_ajax_get_storelisting', 'get_storelisting');



//choose this location FUnctionality

add_action('wp_ajax_nopriv_choose_location', 'choose_location');
add_action('wp_ajax_choose_location', 'choose_location');

function choose_location()
{

    $content = get_the_title($_POST['store_id']) . ', ' . get_field('state', $_POST['store_id']);
    $format_phone = formatPhoneNumber(get_field('phone', $_POST['store_id']));
    $text_phone = get_field('phone', $_POST['store_id']);

    $data = array();

    $data['header_location'] = $content;
    $data['formatphone'] = $format_phone;
    $data['text_phone'] = $text_phone;

    echo json_encode($data);
    wp_die();
}

//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

function location_header()
{
    return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
    <div class="locationOverlay"></div>';
}
add_shortcode('location_code', 'location_header');

add_filter('gform_pre_render_18', 'populate_product_location_form');
//add_filter( 'gform_pre_validation_18', 'populate_product_location_form' );
//add_filter( 'gform_pre_submission_filter_18', 'populate_product_location_form' );
//add_filter( 'gform_admin_pre_render_18', 'populate_product_location_form' );

// add_filter( 'gform_pre_render_19', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_19', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_19', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_19', 'populate_product_location_form' );

add_filter('gform_pre_render_16', 'populate_product_location_form');
add_filter('gform_pre_validation_16', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_16', 'populate_product_location_form');
add_filter('gform_admin_pre_render_16', 'populate_product_location_form');

add_filter('gform_pre_render_24', 'populate_product_location_form');
// add_filter( 'gform_pre_validation_24', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_24', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_24', 'populate_product_location_form' );

add_filter('gform_pre_render_30', 'populate_product_location_form');
add_filter('gform_pre_validation_30', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_30', 'populate_product_location_form');
add_filter('gform_admin_pre_render_30', 'populate_product_location_form');

add_filter('gform_pre_render_53', 'populate_product_location_form');
add_filter('gform_pre_validation_35', 'populate_product_location_form');
add_filter('gform_pre_submission_filter_35', 'populate_product_location_form');
add_filter('gform_admin_pre_render_53', 'populate_product_location_form');



function populate_product_location_form($form)
{

    foreach ($form['fields'] as &$field) {

        // Only populate field ID 12
        if ($field->type != 'select' || strpos($field->cssClass, 'populate-store') === false) {
            continue;
        }

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );

        $locations =  get_posts($args);

        $choices = array();

        foreach ($locations as $location) {


            $title = get_the_title($location->ID);

            $choices[] = array('text' => $title, 'value' => $title);
        }
        wp_reset_postdata();

        // //write_log($choices);

        // Set placeholder text for dropdown
        $field->placeholder = '-- Choose Location --';

        // Set choices from array of ACF values
        $field->choices = $choices;
    }
    return $form;
}

//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
    // //write_log($sql_delete);	
}
add_filter('auto_update_plugin', '__return_false');

function flooring101check_404($url)
{
    $headers = get_headers($url, 1);
    if ($headers[0] != 'HTTP/1.1 200 OK') {
        return true;
    } else {
        return false;
    }
}


wp_clear_scheduled_hook('404_redirection_log_cronjob');
wp_clear_scheduled_hook('404_redirection_301_log_cronjob');
if (!wp_next_scheduled('custom_404_redirection_301_log_cronjob')) {

    wp_schedule_event(time() +  17800, 'daily', 'custom_404_redirection_301_log_cronjob');
}

add_action('custom_404_redirection_301_log_cronjob', 'themecustom_404_redirect_hook');


// custom 301 redirects from  404 logs table
function themecustom_404_redirect_hook()
{
    global $wpdb;
    // //write_log('themecustom_404_redirect_hook in function');

    $table_redirect = $wpdb->prefix . 'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix . 'redirection_groups';

    $data_404 = $wpdb->get_results("SELECT * FROM $table_name");
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;

    if ($data_404) {
        foreach ($data_404 as $row_404) {

            if (strpos($row_404->url, 'carpet') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {
                // //write_log($row_404->url);      

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = flooring101check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $source_url = $row_404->url;
                    $destination_url = '/browse-carpet/';
                    $match_url = rtrim($source_url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');

                    // //write_log( 'carpet 301 added ');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }
            } else if (strpos($row_404->url, 'hardwood') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                //write_log($row_404->url);

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');
                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = flooring101check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $source_url = $row_404->url;
                    $destination_url = '/browse-hardwood/';
                    $match_url = rtrim($source_url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                // //write_log( 'hardwood 301 added ');

            } else if (strpos($row_404->url, 'laminate') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                ////write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = flooring101check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $source_url = $row_404->url;
                    $destination_url = '/browse-laminate/';
                    $match_url = rtrim($source_url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                //write_log( 'laminate 301 added ');


            } else if ((strpos($row_404->url, 'luxury-vinyl') !== false || strpos($row_404->url, 'vinyl') !== false) && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                //write_log($row_404->url);  

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = flooring101check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $source_url = $row_404->url;
                    if (strpos($row_404->url, 'luxury-vinyl') !== false) {
                        $destination_url = '/browse-luxury-vinyl/';
                    } elseif (strpos($row_404->url, 'vinyl') !== false) {
                        $destination_url = '/browse-luxury-vinyl/';
                    }

                    $match_url = rtrim($source_url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                //write_log( 'luxury-vinyl 301 added ');
            } else if (strpos($row_404->url, 'tile') !== false && strpos($row_404->url, 'products') !== false && strpos($row_404->url, 'flooring') !== false) {

                //write_log($row_404->url); 

                $checkalready =  $wpdb->query('Select * FROM ' . $table_redirect . ' WHERE url LIKE "%' . $row_404->url . '%"');

                $url_404 = home_url() . '' . $row_404->url;
                $headers_res = flooring101check_404($url_404);

                if ($checkalready == 0 && $headers_res == 1) {

                    $destination_url = '/browse-ceramic/';
                    $match_url = rtrim($row_404->url, '/');

                    $data = array(
                        "url" => $row_404->url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => "",
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time('mysql'),
                        "status" => "enabled"
                    );

                    $format = array('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');

                    $wpdb->insert($table_redirect, $data, $format);
                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                } else {

                    $wpdb->query('DELETE FROM ' . $table_name . ' WHERE id = "' . $row_404->id . '"');
                }

                //write_log( 'customtile 301 added ');
            }
        }
    }
}


function search_distinct()
{

    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';
}

// Vendor page product list

function brand_products_function($atts)
{

    global $wpdb;

    // "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
    //write_log( $atts);
    $args = array(
        "post_type" =>  array('luxury_vinyl_tile'),
        "post_status" => "publish",
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value',
        "posts_per_page" => 4,
        'meta_query' => array(
            array(
                'key' => 'brand',
                'value' => $atts[0],
                'compare' => '='
            )
        )
    );

    //  write_log( $args);
    // add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
    $the_query = new WP_Query($args);

    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';


    while ($the_query->have_posts()) {
        $the_query->the_post();
        $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');
        $style = "padding: 5px;";

        $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="' . get_the_permalink() . '" title="' . get_the_title() . '">
                    <img class="list-pro-image" src="' . $image . '" alt="' . get_the_title() . '">';

        if (get_field('quick_ship') == 1) {

            $content .= '<div class="bb_special"><p>QUICK SHIP</p></div>';
        }

        $content .= '</a></div>';

        $table_posts = $wpdb->prefix . 'posts';
        $table_meta = $wpdb->prefix . 'postmeta';
        $familycolor = get_field('collection');
        $key = 'collection';

        $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

        //     write_log($coll_sql);

        $data_collection = $wpdb->get_results($coll_sql);


        $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="' . get_the_permalink() . '" title="' . get_the_title() . '" class="collection_text">' . get_field('collection') . '</a> 
                            <a href="' . get_the_permalink() . '" title="' . get_the_title() . '"> <span class="color_text">' . get_field('color') . '</span></a>
                                <span class="brand_text">' . get_field('brand') . ' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>' . count($data_collection) . ' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
    }

    wp_reset_postdata();

    // remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';

    return $content;
}

add_shortcode('brand_listing', 'brand_products_function');


function carpet_brand_products_function($atts)
{

    global $wpdb;

    // "post_type" =>  array('luxury_vinyl_tile', 'laminate_catalog', 'hardwood_catalog', 'tile_catalog', 'carpeting'),
    //write_log( $atts);
    $args = array(
        "post_type" =>  array('carpeting'),
        "post_status" => "publish",
        "order" => "rand",
        "meta_key" => 'collection',
        "orderby" => 'meta_value',
        "posts_per_page" => 4,
        'meta_query' => array(
            array(
                'key' => 'brand',
                'value' => $atts[0],
                'compare' => '='
            )
        )
    );

    //  write_log( $args);
    //add_filter('posts_groupby', 'search_distinct');
    //remove_filter('posts_groupby', 'query_group_by_filter');
    $the_query = new WP_Query($args);

    $content = '<div class="product-plp-grid product-grid swatch related_product_wrapper brandpro_wrapper">				
                <div class="row product-row">';


    while ($the_query->have_posts()) {
        $the_query->the_post();
        $image = swatch_image_product_thumbnail(get_the_ID(), '222', '222');
        $style = "padding: 5px;";

        $content .=  '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="fl-post-grid-post">                
                    <div class="fl-post-grid-image">
                    <a itemprop="url" href="' . get_the_permalink() . '" title="' . get_the_title() . '">
                    <img class="list-pro-image" src="' . $image . '" alt="' . get_the_title() . '">';

        if (get_field('quick_ship') == 1) {

            $content .= '<div class="bb_special"><p>QUICK SHIP</p></div>';
        }

        $content .= '</a></div>';

        $table_posts = $wpdb->prefix . 'posts';
        $table_meta = $wpdb->prefix . 'postmeta';
        $familycolor = get_field('collection');
        $key = 'collection';

        $coll_sql = "select distinct($table_meta.post_id)  
                                FROM $table_meta
                                inner join $table_posts on $table_posts.ID = $table_meta.post_id 
                                WHERE meta_key = '$key' AND meta_value = '$familycolor'";

        //     write_log($coll_sql);

        $data_collection = $wpdb->get_results($coll_sql);


        $content .=     '<div class="fl-post-grid-text product-grid btn-grey">
                            <h4><a href="' . get_the_permalink() . '" title="' . get_the_title() . '" class="collection_text">' . get_field('collection') . '</a> 
                            <a href="' . get_the_permalink() . '" title="' . get_the_title() . '"> <span class="color_text">' . get_field('color') . '</span></a>
                                <span class="brand_text">' . get_field('brand') . ' </span> </h4>
                        </div>    
                        
                        <div class="product-variations1"> <h5>' . count($data_collection) . ' COLORS AVAILABLE</h5></div>

                </div>
            </div>';
    }

    wp_reset_postdata();

    // remove_filter('posts_groupby', 'search_distinct');                  
    $content .= '</div>
            </div>';

    return $content;
}

add_shortcode('carpet_brand_listing', 'carpet_brand_products_function');

function my_cache_lifetime($seconds)
{
    return 5184000; // 10 day. Default: 3600 (one hour)
}
add_filter('facetwp_cache_lifetime', 'my_cache_lifetime');




//single instock post type template

// add_filter( 'single_template', 'childtheme_get_custom_post_type_template' );

function childtheme_get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {

        $single_template = get_stylesheet_directory() . '/product-listing-templates/single-' . $post->post_type . '.php';
    }
    return $single_template;
}

//theme loop template changes for instock PLP
add_filter('facetwp_template_html', function ($output, $class) {

    $prod_list = $class->query;
    ob_start();

    $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new.php';

    require_once $dir;
    return ob_get_clean();
}, 10, 2);

add_filter('facetwp_facet_orderby', function ($orderby, $facet) {

    global $wp_query;
    $post = $wp_query->post;
    $slug = get_post_field('post_name', @$post->post_parent);
    $instock = get_post_meta('in_stock', @$post->ID);

    if ('brand' == $facet['name']) {

        $product_cat = array(
            'browse-carpet' => 'FIELD(f.facet_display_value, "Karastan","Mohawk","Fabrica", "Masland","Dixie Home","Engineered Floors","Shaw","Shaw Floors","Stanton","Antrim","Godfrey Hirst","Anderson Tuftex","Dream Weaver","Philadelphia Commercial", "Anderson Tuftex Builder", "Shaw Builder Flooring", "Pentz Commercial", "Phenix", "Kane","Nourison","Beaulieu","Lasting Luxury","5th And Main","Karastan Gallery Des","Karastan Wool","Mohawk Residential","Signature Series-hzn","Aladdin Commercial","Dreamweaver","Godfrey Hirst Na")',
            'browse-hardwood' => 'FIELD(f.facet_display_value, "Robbins", "Mohawk Hard Surface", "Aladdin Commercial", "Preverco","Biyork", "Shaw Floors", "Anderson Tuftex")',
            'browse-laminate' => 'FIELD(f.facet_display_value, "Mohawk Hard Surface", "Aladdin Commercial","Pergo", "Revwood Premier","Revwood Plus","Revwood Select","Revwood", "Mannington","Mohawk")',
            'browse-luxury-vinyl' => 'FIELD(f.facet_display_value, "Oceanside","COREtec","Mohawk","Pergo","Portico","Aladdin Commercial","Shaw Floors","Philadelphia Commercial","Lions Floor","Karastan","Lasting Luxury")',
            'browse-ceramic' => 'FIELD(f.facet_display_value, "Daltile", "American Olean", "Emser", "Marazzi", "Shaw Floors")'
        );

        $orderby = @$product_cat[$slug];
    }
    if (@$instock == '') {
        return $orderby;
    }
}, 10, 2);

//Cron job for sync catalog for all mohawk categories

if (! wp_next_scheduled('sync_mohawk_catalog_all_categories')) {

    wp_schedule_event(strtotime("last Sunday of " . date('M') . " " . date('Y') . ""), 'monthly', 'sync_mohawk_catalog_all_categories');
}


//add_action( 'sync_mohawk_catalog_all_categories', 'mohawk_product_sync', 10, 2 );

function mohawk_product_sync()
{

    write_log("Only mohawk Catalog sync is running");

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';
    $table_meta = $wpdb->prefix . 'postmeta';
    $product_json =  json_decode(get_option('product_json'));

    $brandmapping = array(
        "hardwood" => "hardwood_catalog",
        "laminate" => "laminate_catalog",
        "lvt" => "luxury_vinyl_tile",
        "tile" => "tile_catalog"
    );

    foreach ($brandmapping as $key => $value) {

        $productcat_array = getArrayFiltered('productType', $key, $product_json);


        foreach ($productcat_array as $procat) {

            if ($procat->manufacturer == "Mohawk") {

                $permfile = $upload_dir . '/' . $value . '_' . $procat->manufacturer . '.json';
                $res = SOURCEURL . get_option('SITE_CODE') . '/www/' . $key . '/' . $procat->manufacturer . '.json?' . SFN_STATUS_PARAMETER;
                $tmpfile = download_url($res, $timeout = 900);

                if (is_file($tmpfile)) {
                    copy($tmpfile, $permfile);
                    unlink($tmpfile);
                }

                $sql_delete = "DELETE p, pm FROM $table_posts p INNER JOIN $table_meta pm ON pm.post_id = p.ID  WHERE p.post_type = '$value' AND pm.meta_key = 'manufacturer' AND  pm.meta_value = 'Mohawk'";
                write_log($sql_delete);
                $delete_endpoint = $wpdb->get_results($sql_delete);
                write_log("mohawk product deleted");
                //exit;

                write_log('auto_sync - Shaw catalog - ' . $key . '-' . $procat->manufacturer);
                $obj = new Example_Background_Processing();
                $obj->handle_all($value, $procat->manufacturer);

                write_log('Sync Completed - ' . $procat->manufacturer);
            }
        }

        write_log('Sync Completed for all  ' . $key . ' brand');
    }
}
